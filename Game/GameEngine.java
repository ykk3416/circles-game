    import javax.swing.*;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;

/*
Possible improvements for future versions:
1.) Rather than having the entire program run linearly, have it run like a collection of slides (CardLayout may help here).
2.) Change layout manager of menu screens from BorderLayout and FlowLayout to GridBagLayout and CardLayout. Or consider using Netbeans and GroupLayout for easy drag-and-drop customization.
3.) Add sound effects and possibly animation effects for transition from one screen to another.
4.) Include hi-score table as just one of the components of a container so that buttons can be added to that menu screen.
5.) Maybe add various difficulty levels for the user to select.
6.) Change graphics of the circle and the background of the JPanel to something more appealing.
7.) Prohibit the user from pressing "submit" multiple times in the hi-score menu.
 */


public class GameEngine implements Serializable  {
    
    private int lives = 3;
    private int points;
    private int timeCounter = 2000;
    
    public static final int PANELX = 400;
    public static final int PANELY = 400;
    JFrame frame;
    JPanel panel;
    JLabel pointLabel;
    JLabel lifeLabel;
    
    Circles circle;
    transient CircleComponent component;
    transient Thread t;
    transient Thread t2;
    JTextField field;
    String name;
    Object[][] data = new Object[10][2];
    
    ArrayList<GameEngine> users = new ArrayList<GameEngine>(10);
    
    
    public static void main (String [] args) {
        new GameEngine().runTitle();
    }
    
    //First method to run. Introduces the title screen.
    public void runTitle() {
        
        //Assigns frame as new JFrame.
        frame = new JFrame();
        
        //Assigns label as new JLabel for the title screen.
        JLabel label = new JLabel("Epic Circles", SwingConstants.CENTER);
        label.setFont(new Font("Serif", Font.BOLD, 48));
        label.setForeground(Color.red);
        
        //Assigns button and button2 as start and quit buttons and adds listeners to them.
        JButton button = new JButton("Start");
        JButton button2 = new JButton("Quit");
        button.addActionListener(new ButtonListener());
        button2.addActionListener(new ButtonListener2());
        
        //Alligns buttons in a BoxLayout.
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(button);
        buttonPanel.add(button2);
        
        //Places buttons and title screen label on the frame.
        frame.getContentPane().add(BorderLayout.SOUTH, buttonPanel);
        frame.getContentPane().add(BorderLayout.CENTER, label);
        
        //Puts in dimensions for frame and shows it. Had to set frame large enough so that the point/life labels don't cover the panel.
        frame.setSize(700, 700);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    //After the user clicks the "start" button, this method runs.
    public void setUp() {
        
        //Removes components from title screen and revalidates frame.
        frame.getContentPane().removeAll();
        frame.getContentPane().revalidate();
        
        //Assigns panel as new JPanel. Sets dimensions for new JPanel.
        panel = new JPanel();
        panel.setLayout(new BorderLayout()); //Was originally going to have a separate JComponent that drew the circle and provided set imensionsadded FlowLayout forces positioning and does not process paintComponent for some reason, and BorderLayout forces component to encompass the entire panel.
        panel.setSize(PANELX, PANELY);
        
        //Assigns component as new CircleComponent and adds it to the panel.
        component = new CircleComponent();
        panel.add(component);
        
        //Assigns lifeLabel and pointLabel as new JLabels. These labels are added to let user know the numbers of points/lives he/she have.
        lifeLabel = new JLabel("Lives: "+lives);
        pointLabel = new JLabel("Points: "+points);
        
        //Adds panel and point/life labels to JFrame.
        frame.getContentPane().add(BorderLayout.CENTER, panel);
        frame.getContentPane().add(BorderLayout.EAST, pointLabel);
        frame.getContentPane().add(BorderLayout.WEST, lifeLabel);
        
        //Not sure why these are not needed.
        /*frame.setSize(500, 500);
         frame.setVisible(true);
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);*/
    }
    
    //The Jframe updates the point/life labels with new values every time a circle disappears or is clicked.
    public void updateCounters() {
        
        //Removes older point/life labels.
        frame.getContentPane().remove(pointLabel);
        frame.getContentPane().remove(lifeLabel);
        
        //Assigns point/life labels as new JLabels with updated points/lives.
        lifeLabel = new JLabel("Lives: "+lives);
        pointLabel = new JLabel("Points: "+points);
        
        //Adds point/life labels with JLabels with updated points/lives to current JFrame.
        frame.getContentPane().add(BorderLayout.EAST, pointLabel);
        frame.getContentPane().add(BorderLayout.WEST, lifeLabel);
        frame.getContentPane().revalidate();
    }
    
    //Restarts the game with default point/life/time counter values after user clicks the "restart" button.
    public void restart() {
        points = 0;
        timeCounter = 2000;
        lives = 3;
        setUp();
        
    }
    
    //Shows user a GameOver screen if the user lost three lives.
    public void GameOver() {
        
        //Wipes JFrame.
        frame.getContentPane().removeAll();
        frame.getContentPane().revalidate();
        
        //Assigns label and pointLabel as new JLabels. Instance label says "Game Over" and text is placed in the middle of the component.
        JLabel label = new JLabel("Game Over", SwingConstants.CENTER);
        label.setFont(new Font("Serif", Font.BOLD, 48));
        label.setForeground(Color.red);
        JLabel pointLabel = new JLabel("Points: "+points);
        
        //Assigns button and button2 as new JButtons "Exit" and "Restart", respectively. Adds appropriate listeners to each.
        JButton button = new JButton("Exit");
        JButton button2 = new JButton("Restart");
        JButton button3 = new JButton("High Scores");
        button.addActionListener(new ButtonListener2());
        button2.addActionListener(new ButtonListener3());
        button3.addActionListener(new ButtonListener4());
        
        //Aligns the components to a BoxLayout.
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(button);
        panel.add(button2);
        panel.add(button3);
        panel.add(pointLabel);
        
        //Adds all of the mentioned components to the JFrame.
        frame.getContentPane().add(BorderLayout.CENTER, label);
        frame.getContentPane().add(BorderLayout.SOUTH, panel);
        
        //I don't know why I need this, since I already set the it visible in runTitle().
        frame.setVisible(true);
    }
    
    //Brings up a menu to save points and username.
    public void saveScores() {
        
        //Wipes JFrame.
        frame.getContentPane().removeAll();
        frame.getContentPane().revalidate();
        
        //Assigns new JPanel as container.
        panel = new JPanel();
        //panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        //Assigns and sets up components for the JPanel with respective ActionListeners.
        JLabel label = new JLabel("UserName: ");
        field = new JTextField(20);
        field.setMaximumSize(field.getPreferredSize());
        JButton button = new JButton("Submit");
        JButton button2 = new JButton("Hi-Scores");
        JButton button3 = new JButton("Exit");
        
        //Keeping things aligned.
        /*label.setAlignmentX(Component.CENTER_ALIGNMENT);
        field.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button2.setAlignmentX(Component.CENTER_ALIGNMENT);
        button3.setAlignmentX(Component.CENTER_ALIGNMENT);
        */
        
        //Giving components listeners.
        button.addActionListener(new ButtonListener5());
        button2.addActionListener(new ButtonListener6());
        button3.addActionListener(new ButtonListener2());
        field.addActionListener(new ButtonListener5());
        
        //Attempt to force the "submit" button to only accept once submission. This doesn't work though.
        if (button.getModel().isPressed()) {
            button.setEnabled(false);
        }
        
        //Adds all components to the JPanel container.
        panel.add(label);
        panel.add(field);
        panel.add(button);
        panel.add(button2);
        panel.add(button3);
        
        //Finally adds JPanel to the JFrame.
        frame.getContentPane().add(panel);
    }
    
    //Saves hi-score file.
    public void saveFile() {
        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("highscores.ser"));
            os.writeObject(users);
            os.close();
        } catch(IOException ex) {ex.printStackTrace(); System.out.println("couldn't save file");}
    }
    
    //Loads up the hi-score file. Must supress warning since casting the deserialized object as a generic results in an "unchecked" warning when compiling.
    @SuppressWarnings("unchecked")
    public void loadFile() {
        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("highscores.ser"));
            users = (ArrayList<GameEngine>) is.readObject();
            System.out.println("Save file read");
            System.out.println("Total number of users: "+users.size());
            is.close();
            
        //Method runs after the user clicks the "submit" button. If there is no existing save data, then this method will run saveFile() and repeat its own.
        } catch (Exception ex) {
            System.out.println("Save file not found, so generating a new one...");
            saveFile();
            loadFile();
        }
    }
    
    //Runs when the user clicks "submit" on the save menu.
    public void submit() {
        
        //Retrieves inputted name and saves it as a string. Then runs loadFile().
        name = field.getText();
        loadFile();
    
    //Saved list undergoes several checks to see if it should accept the incoming user.
    search:
        if (!users.isEmpty()) { //There's at least one submitted value.
            for (int i = 0; i < users.size(); i++) { //Scan through each element.
                if (users.get(i).getPoints() < points) //If an already submitted element's points are less than the submitting element's points.
                {
                    users.add(i, this);
                    System.out.println("User replacing rank of submitted element");
                    break search;
                }
                
                if (i == 9) { //If at the last iteration of the list and still unable to replace an existing user. Hi-score list will only hold a total of 10 users.
                    System.out.println("Score too low");
                    panel.add(new JLabel("Score too low. Cannot update high score."));
                    return;
                }
                
                System.out.println("Moving to next element..."); //Moving on to compare with the next user in the list.
            } //End of for loop.
            
            if (users.size() < 10) { //If unable to replace an existing user of the list and there are less than 10 users in the list.
                users.add(this);
                System.out.println("User added to open slot");
                break search;
            }
        } //End of search block.
        
        if (users.isEmpty()) { //If ArrayList is empty.
            users.add(0, this);
            System.out.println("A new beginning...");
        }
        
        if (users.size() > 10) { //So that size never goes over 10.
            users.remove(9);
            System.out.println("Last ranking user deleted...Get the FUCK OUT DA WHEYYY");
        }
        
        panel.add(new JLabel("Success!"));
        
        //Saves the new list.
        saveFile();
    }
    
    //Converts the ArrayList into a 2D array that the JTable can read.
    public void extractData() {
        GameEngine[] gArray = users.toArray(new GameEngine[users.toArray().length]);
        for (int i = 0; i<gArray.length; i++) {
            data[i][0] = gArray[i].getName();
            data[i][1] = gArray[i].getPoints();
        }
    }
    
    //Runs when the user clicks the "Hi-Scores" button.
    public void showHiScores() {
        //Need to load the most updated ArrayList.
        loadFile();
        
        //Need to extract the data from the ArrayList to a form that JTable can understand.
        extractData();
        
        //Wipes JFrame.
        frame.getContentPane().removeAll();
        frame.getContentPane().revalidate();
        
        //Assigns new JPanel and sets layout to BorderLayout for aesthetics.
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setSize(PANELX, PANELY);
        
        //Assigns new JTable and set it to a new JScrollPane
        JTable table = new JTable(new MyTableModel());
        table.setFillsViewportHeight(true);
        JScrollPane pane = new JScrollPane(table);
        
        //Adds JScrollPane to JPanel.
        panel.add(pane, BorderLayout.CENTER);
        frame.getContentPane().add(panel);
        frame.setVisible(true);
    }
    
    //Returns points.
    Integer getPoints() {
        return points;
    }
    
    //Returns name.
    String getName() {
        return name;
    }
    
    
    //All the buttons
    class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //Starts game
            setUp();
        }
    }
    
    class ButtonListener2 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //Exits game
            System.exit(0);
        }
    }
    
    class ButtonListener3 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //Restarts game
            restart();
        }
    }
    
    class ButtonListener4 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //Saves current ArrayList.
            saveScores();
        }
    }
    
    class ButtonListener5 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //Checks the ArrayList to see if submitted user can be accepted.
            submit();
        }
    }
    
    class ButtonListener6 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //Opens hi-scores.
            showHiScores();
        }
    }
    
    //Model for JTable.
    class MyTableModel extends AbstractTableModel {
        private String[] columnNames = {"Username", "Score"};
        private Object[][] tableData = data;
        
        public int getColumnCount() {
            return columnNames.length;
        }
        
        public int getRowCount() {
            return tableData.length;
        }
        
        public String getColumnName(int col) {
            return columnNames[col];
        }
        
        public Object getValueAt(int row, int col) {
            return tableData[row][col];
        }
        
        //Returns Class values.
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
        
        //Cannot be editable.
        public boolean isCellEditable(int row, int col) {
            return false;
        }
        
        
    }
    //Draws a red, clickable circle on the previously assigned JPanel by using coordinates from the Circle class.
    class CircleComponent extends JComponent {
        MouseAdapter adapter = new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
        
        //Removes circle from JPanel and starts a countdown for the appearance of a new circle if the user clicks within the coordinates of the circle's position and size.
        if(circle.getShape().contains(e.getX(),e.getY())) {
        panel.remove(component);
        panel.repaint();
        t = new Thread(new Timer());
        t.start();
    }
}
};

CircleComponent() {
//debug System.out.println("Made new component");

//Assigns circle as new Circles. This runs the Circles no-arg constructor, which assigns new coordinates for size and position of the new circle.
circle = new Circles();
addMouseListener(adapter);

//Every time a new CircleComponent is made, Thread t2 gets assigned a new Thread with Runnable Timer2 and starts up. This starts a countdown, and if the user fails to click on the circle, then it disappears and a new circle with different coordinates for position and size is placed on the panel.
t2 = new Thread(new Timer2());
t2.start();
}

//Draws the circle with appropriate coordinates from Circles class.
public void paintComponent(Graphics gg) {
Graphics2D g = (Graphics2D) gg;
g.setColor(Color.red);
g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
g.draw(circle.getShape());
}
}

//Timer starts after the user clicks on the circle.
class Timer implements Runnable {
    public void run() {
        t2.interrupt(); //Interrupts and stops the countdown for the disappearance of the circle.
        //debug System.out.println("starting Timer1...");
        
        //Timer that determines how long the CircleComponent would stay OFF the screen
        try {
            Thread.sleep(timeCounter);
        } catch(InterruptedException ex) { //debug System.out.println("whops");
        }
        
        //Updates points and timeCounter, and makes new CircleComponent if circle is clicked.
        points++;
        timeCounter = (int) (timeCounter - timeCounter*.10);
        component = new CircleComponent();
        updateCounters();
        
        //Adds new CircleComponent to JPanel.
        panel.add(component);
        panel.revalidate();
        //debug System.out.println(String.valueOf(points+" points"));
        //debug System.out.println(String.valueOf(timeCounter+" ms"));
    }
}

//Timer starts after a new CircleComponent is produced.
class Timer2 implements Runnable {
    public void run() {
        
        //Timer that determines how long the CircleComponent would stay ON the screen
        try {
            Thread.sleep(timeCounter);
        } catch(InterruptedException ex) {
            //debug System.out.println("returned");
            return; //If user clicks on the circle, timer gets interrupted.
        }
        
        //Updates lives, and makes new CircleComponent if circle is not clicked on time.
        lives--;
        System.out.println(lives+ "lives");
        if (lives==0) {GameOver(); return;}
        //debug System.out.println("starting Timer2...");
        //debug System.out.println(timeCounter);
        updateCounters();
        panel.removeAll();
        component = new CircleComponent();
        
        //Adds new CircleComponent to JPanel.
        panel.add(component);
        panel.revalidate();
    }
    
}

}