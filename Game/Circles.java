import java.awt.geom.Ellipse2D;
import java.io.*;

public class Circles implements Serializable {
    private static final double MAXSIZE = 50;
    private static final double MINSIZE = 20;
    private static final double RANGEX = MAXSIZE - MINSIZE;
    private double size;
    private double positionX;
    private double positionY;
    private Ellipse2D.Double shape;
    
    
    public Circles() {
        appear();
        shape = new Ellipse2D.Double(getPositionX(), getPositionY(), getSize(), getSize());

    }
    
    public Ellipse2D.Double getShape() {
        return shape;
    }
    
    public void setSize() {
        size = (RANGEX*Math.random()+MINSIZE);
    }
    
    public void setPosition() {
        positionX = (GameEngine.PANELX*Math.random());
        positionY = (GameEngine.PANELY*Math.random());
    }
    
    public void appear() {
        setSize();
        setPosition();
    }
    
    public double getPositionX() {
        return positionX;
    }
    
    public double getPositionY() {
        return positionY;
    }
    
    public double getSize() {
        return size;
    }

    

}